# CHANGELOG.md

## 0.2.0

New features:

 - GUI for reprocessing XRPD data: `id31-integrate` command
 - New streamline autocalib workflow and script: `streamline-autocalib` command
 - Orange widget for `FlatFieldFromEnergy` task
 - Include id31 workflows and scripts: `id31-reprocessed-xrpd` and `streamline-reprocess`

Changes:
 - Use pyproject.toml for project configuration
 - Update command line option names for consistency

## 0.1.2

Bug fixes:

- `FlatFieldFromEnergy`: handle negative values

## 0.1.1

Bug fixes:

- Fix toml requirements

## 0.1.0

New features:
    - task for calculating flat-field from beam energy
