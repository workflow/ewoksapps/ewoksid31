# ewoksid31

Data processing workflows for ID31.

## Getting started

Install requirements

```bash
pip install ewoksid31
```

## Documentation

- General documentation: [ewoksid31.readthedocs.io](https://ewoksid31.readthedocs.io/)
- GUI usage and reprocessing details: [Confluence - GUI for reprocessing XRPD data](https://confluence.esrf.fr/display/ID31KB/GUI+for+reprocessing+XRPD+data)


