from __future__ import annotations

from typing import List, Optional
import os
import h5py
from silx.gui import qt


def generateInputs(
    newFlat: str,
    oldFlat: str,
    energy: float,
    pyfaiConfig: dict,
    pyfaiMethod: str,
    datasetFilename: str,
    scanNumber: int,
    monitorName: str,
    referenceCounts: int,
    detectorName: str,
    outputDirectory: str,
    sigmaClippingThreshold: Optional[float],
    asciiExport: bool,
) -> List[dict]:
    """
    Generate input parameters for the EWOKS workflow.
    """
    baseDirName = os.path.splitext(os.path.basename(datasetFilename))[0]
    outputFilePathH5 = os.path.join(outputDirectory, f"{baseDirName}.h5")

    outputAsciiFileTemplate = os.path.join(
        f"{baseDirName}_{scanNumber:04d}_{detectorName}_%04d.xye",
    )
    outputArchiveFilename = os.path.join(
        outputDirectory, "export", f"{baseDirName}_{scanNumber:04d}_{detectorName}.zip"
    )

    integrationOptions = {
        "method": pyfaiMethod,
    }
    if sigmaClippingThreshold is not None:
        integrationOptions["extra_options"] = {
            "thres": sigmaClippingThreshold,
            "max_iter": 10,
            "error_model": "azimuthal",
        }
        integrationOptions["integrator_name"] = "sigma_clip_ng"

    inputs = [
        {
            "task_identifier": "FlatFieldFromEnergy",
            "name": "newflat",
            "value": newFlat,
        },
        {
            "task_identifier": "FlatFieldFromEnergy",
            "name": "oldflat",
            "value": oldFlat,
        },
        {
            "task_identifier": "FlatFieldFromEnergy",
            "name": "energy",
            "value": energy,
        },
        {
            "task_identifier": "PyFaiConfig",
            "name": "filename",
            "value": pyfaiConfig,
        },
        {
            "task_identifier": "PyFaiConfig",
            "name": "integration_options",
            "value": integrationOptions,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "filename",
            "value": datasetFilename,
        },
        {"task_identifier": "IntegrateBlissScan", "name": "scan", "value": scanNumber},
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "output_filename",
            "value": outputFilePathH5,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "monitor_name",
            "value": monitorName,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "reference",
            "value": referenceCounts,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "maximum_persistent_workers",
            "value": 1,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "retry_timeout",
            "value": 3600,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "detector_name",
            "value": detectorName,
        },
        {
            "task_identifier": "SaveNexusPatternsAsAscii",
            "name": "enabled",
            "value": asciiExport,
        },
        {
            "task_identifier": "SaveNexusPatternsAsAscii",
            "name": "output_filename_template",
            "value": outputAsciiFileTemplate,
        },
        {
            "task_identifier": "SaveNexusPatternsAsAscii",
            "name": "output_archive_filename",
            "value": outputArchiveFilename,
        },
    ]

    outputPathJson = os.path.join(
        outputDirectory, f"{baseDirName}_{scanNumber}_{detectorName}.json"
    )

    return {
        "inputs": inputs,
        "convert_destination": outputPathJson,
    }


def extractScanNumber(h5path: str) -> int:
    """
    Extracts the scan number from the h5path from a selected node.

    Example: '/2.1/measurement/p3" -> returns 2'
    """
    parts = h5path.split("/")
    if len(parts) > 1:
        scanNumberPart = parts[1].split(".")[0]
        try:
            return int(scanNumberPart)
        except ValueError:
            pass
    return -1


def generateUniquePath(basePath: str) -> str:
    """
    Generates a unique path by adding an incremantal suffix if necessary.

    Args:
        basePath: Base path (file or directory)

    Returns:
        unique path
    """
    isFile = os.path.isfile(basePath)
    dirName = os.path.dirname(basePath)
    baseName, ext = (
        os.path.splitext(os.path.basename(basePath)) if isFile else (basePath, "")
    )

    parts = baseName.rsplit("_", 1)
    if len(parts) == 2 and parts[1].isdigit():
        baseName, counter = parts[0], int(parts[1]) + 1
    else:
        counter = 1

    generatedName = os.path.join(dirName, f"{baseName}_{counter}{ext}")
    while os.path.exists(generatedName):
        counter += 1
        generatedName = os.path.join(dirName, f"{baseName}_{counter}{ext}")
    return generatedName


def getScanEnergy(h5Filename: str, scanNumber: int) -> Optional[float]:
    """
    Retrieves the energy for a specific scan in an HDF5 file.

    Args:
        h5Filename: Path to the HDF5 file.
        scanNumber: Scan number to retrieve energy for.

    Returns:
        Energy value for a given scan.
    """
    scanPath = f"{scanNumber}.1/instrument/positioners"

    with h5py.File(h5Filename, "r") as h5file:
        if scanPath in h5file and "energy" in h5file[scanPath].keys():
            return h5file[scanPath]["energy"][()]

    return None


class FilenameCompleterLineEdit(qt.QLineEdit):
    """
    Heritage from QLineEdit widget that provides autocompletion for file paths.

    This widget uses a QFileSystemModel to suggest file and directory paths,
    starting from the root of the filesystem.
    """

    def __init__(self, parent: qt.QWidget | None = None, **kwargs) -> None:
        super().__init__(parent=parent, **kwargs)

        completer = qt.QCompleter()
        model = qt.QFileSystemModel(completer)
        model.setOption(qt.QFileSystemModel.DontWatchForChanges, True)
        model.setRootPath("/")

        completer.setModel(model)
        completer.setCompletionRole(qt.QFileSystemModel.FileNameRole)
        self.setCompleter(completer)
