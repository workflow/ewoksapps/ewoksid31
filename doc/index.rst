ewoksid31 |version|
===================

*ewoksid31* provides data processing workflows for ID31.

*ewoksid31* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
and the ID31 staff of the `European Synchrotron <https://www.esrf.eu/>`_.


Getting started
----------------

This guide helps you to install `ewoksid31` and get started quickly.

Install requirements:

.. code-block:: bash

    pip install ewoksid31


Launch the graphical user interface for XRPD reprocessing:

.. code-block:: bash

    id31-integrate


These command-line tools are available for advanced processing:

.. code-block:: bash

    id31-reprocess-xrpd --help
    streamline-autocalib --help
    streamline-reprocess --help


Documentation
-------------

.. toctree::
    :maxdepth: 2

    cli
    tasks
    api