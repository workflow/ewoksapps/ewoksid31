Command Line Interface (CLI)
============================

`ewoksid31` project provides the following command-line tools.


id31-integrate (GUI)
---------------------
This command launches a graphical user interface for data processing.

.. code-block:: bash

    id31-integrate

.. program-output:: id31-integrate --help


id31-reprocess-xrpd
--------------------
.. program-output:: id31-reprocess-xrpd --help

streamline-autocalib
---------------------
.. program-output:: streamline-autocalib --help

streamline-reprocess
---------------------
.. program-output:: streamline-reprocess --help
